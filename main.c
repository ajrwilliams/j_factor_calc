/*  
-------------------------------------------------
This file is part of J factor calc 
Andrew Williams, 2016
-------------------------------------------------
*/

#include <stdio.h>
#include "J_factor_calc.h"
#include "regions.h"
#include "math.h"

//*****************************************************************
//** Example main program to calculate J factors for a some regions
//*****************************************************************


int main(int argc,char** argv)
{
  //initialise parameters for the DM profile.
 rhoLocal=0.4;
 rLocal=8.5;
 NFWrs=20.0;
 NFWgamma=1.28;
 EINalpha = 0.17;
 EINrs=20.0;

 //define variables for the regions.
 double ringOffset = 1.42* M_PI/180.0;
 double ringR1 = 0.55* M_PI/180.0;
 double ringR2 = 2.88* M_PI/180.0;
 double ringRcut = 1.36* M_PI/180.0;
 double ringInnerRegion = 1.0* M_PI/180.0;
 double bmask = 0.3* M_PI/180.0;
 //set parameters to be passed to the region functions
 double params[6] = {ringOffset, ringR1, ringR2, bmask, ringRcut, ringInnerRegion};
 
 //define ranges of integration 
  double brange[2] = {-1.46, 4.3};
  double lrange[2] = {-2.88, 2.88};
 

 double solidangle, solidangle2;
 double intresult2,intresult, intresult_fast, intresult2_fast ;
 int calc_solid=1;

 //Calculate Einasto J factor for inner region using simple MC
 intresult = Jintegrator(&ONregion_outer, params, &JEIN, lrange,brange, 50000, &solidangle);    
   printf("Method   :      J     Solid angle\n");
   printf("Simple MC: %.4E %.4E\n", intresult, solidangle);

   //Calculate Einasto J factor using VEGAS routine
   JVEGASintegrator(&ONregion_outer, params, &JEIN, lrange, brange, 50000, calc_solid, &solidangle, &intresult);
   printf("VEGAS MC: %.4E %.4E\n", intresult, solidangle);
   

   //Perform a loop over different values of gamma parameter
   //compare fast interpolation to full result for two regions
   calc_solid=0;
   printf("  gamma   |   J_ON_f   |  J_OFF_f  |    J_ON    |   J_OFF\n");
   for (NFWgamma = 1.1; NFWgamma <= 1.4; NFWgamma+=0.05){
     JNFW_fast_init(100, 0.5, 4.3);
     JVEGASintegrator(&ONregion, params, &JNFW_fast, lrange, brange, 50000, calc_solid, &solidangle, &intresult_fast);
     JVEGASintegrator(&OFFregion, params, &JNFW_fast, lrange, brange, 50000, calc_solid, &solidangle2, &intresult2_fast);
     JVEGASintegrator(&ONregion, params, &JNFW, lrange, brange, 50000, calc_solid, &solidangle, &intresult);
     JVEGASintegrator(&OFFregion, params, &JNFW, lrange, brange, 50000, calc_solid, &solidangle2, &intresult2);
     printf(" %lf   %.4E   %.4E   %.4E   %.4E \n", NFWgamma, intresult_fast, intresult2_fast, intresult, intresult2);
     JNFW_fast_free();
   }
 

}
