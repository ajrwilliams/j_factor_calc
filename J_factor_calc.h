/*  
-------------------------------------------------
This file is part of J factor calc 
Andrew Williams, 2016
-------------------------------------------------
*/

extern double rhoLocal;
extern double rLocal;
extern double NFWrs;
extern double NFWgamma;
extern double EINrs;
extern double EINalpha;


//--------------------------------
//Functions for defining region
//--------------------------------
int Box(double l, double b, double l1, double b1, double l2, double b2);
int GalacticMask(double b, double bmask);
int InAnnulus(double r, double ringR1, double ringR2);
int NOregion(double l, double b);
double offSetRadius(double l, double b, double ringOffset);
double galacticRadius(double l, double b);
double Gr(double d, double l, double b);

//--------------------------------
//Functions for definition of DM density and J factors
//--------------------------------
double JNFW(double l, double b);
double NFWlosWrapper(double d, void* params);
double NFWlos(double d, double l, double b);
double rhoNFW(double r);
double JEIN(double l, double b);
double EINlosWrapper(double d, void* params);
double rhoEIN(double r);
double EINlos(double d, double l, double b);

//--------------------------------
//Interpolation for faster calculation
//--------------------------------
void JEIN_fast_free(void);
void JEIN_fast_init(int N, double theta_min, double theta_max);
double JEIN_fast(double l, double b);
void JNFW_fast_free(void);
void JNFW_fast_init(int N, double theta_min, double theta_max);
double JNFW_fast(double l, double b);

//--------------------------------
//Integrators
//--------------------------------
double Jintegrator(int (*regionFunc)(double, double , void*), double* params, double Jfun(double, double), double* lrange, double* brange, int samples, double* solidAngleOut);
void JVEGASintegrator(int (*regionFunc)(double, double, void*), double* params, double (*Jfun)(double, double), double* lrange, double* brange, int samples, int doSoildAngle, double *solidAngleOut, double *result);
void Jintegrator_two(int (*regionFuncON)(double, double, void*),int (*regionFuncOFF)(double, double, void*), double* params, double Jfun(double, double), double* lrange, double* brange, int samples, double* solidAngleOut, double* result);




