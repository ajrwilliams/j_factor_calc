/*  
-------------------------------------------------
This file is part of J factor calc 
Andrew Williams, 2016
-------------------------------------------------
*/

#include "J_factor_calc.h"
#include <stdio.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_monte.h>
#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_monte_miser.h>
#include <gsl/gsl_monte_vegas.h>
#include <time.h>

//--------------------------------
//Parameters for DM density
//--------------------------------
double rhoLocal;
double rLocal;
double NFWrs;
double NFWgamma;
double EINalpha;
double EINrs;
double EINnorm;


//--------------------------------
//  Region Definition functions
//--------------------------------
int Box(double l, double b, double l1, double b1, double l2, double b2){
  //define a box region from (l1, b1) to (l2, b2)
  if ((l > l1 && l < l2) && (b > b1 && b < b2))
    return 1;
  else
    return 0;

}

int GalacticMask(double b, double bmask){
  //apply a mask to region around galactic plane up to bmask
  if (fabs(b) > bmask)
    return 1;
  else
    return 0;
}

int InAnnulus(double r, double ringR1, double ringR2){
  //check if point is in an annulus defined from R1 to R2
  if (r > ringR1 && r < ringR2)
    return 1;
  else
    return 0;
}

double offSetRadius(double l, double b, double ringOffset){
  //Return radius coordinate in radians offset in degrees from galactic centre 
  return acos(cos(l)*cos(b - ringOffset*M_PI/180.0));
}

double galacticRadius(double l, double b){
  //Radial coordinate in radians from galactic centre
  return acos(cos(l)*cos(b));

}

int NOregion(double l, double b){
  //No region defined always return 1
  return 1;
}

//--------------------------------
//DM density functions and J factor calculation
//--------------------------------
double JNFW(double l, double b){
  //Returns J factor for NFW profile by calculating integral along line of sight
  
  gsl_function F;
  F.function = &NFWlosWrapper;
  double params[2];
  params[0] = l;
  params[1] = b;
  F.params = params;
  double result, error;
  
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (1000);
  gsl_integration_qag (&F, 0, 1000, 0, 1e-3, 15,
			 1000, w, &result, &error); 
  gsl_integration_workspace_free (w);
  return cos(b)*result;
}

double NFWlosWrapper(double d, void* params){
  //wrapper for GSL integration
  double *x = (double*)params;
  double l = x[0];
  double b = x[1];
  return NFWlos(d, l, b);
}

double NFWlos(double d, double l, double b){
  //The line of sight integrand for NFW profile
  double conversion =3.08567758e18 * 1e3;
  double r = Gr(d,l,b);
  double rho = rhoNFW(r);
  return pow(rho,2)*conversion;
}


double Gr(double d, double l, double b){
  //conversion to distance from galactic centre from direction and distance along line of sight
  return sqrt(pow(d*cos(l)*cos(b) - rLocal,2) + pow(d*sin(l)*cos(b),2) + pow(d*sin(b),2));
}

double rhoNFW(double r){
  //The DM density as a function of radial coordinate from GC
  //double norm = pow(1 + rLocal/NFWrs, -NFWgamma)*pow(rLocal/NFWrs,NFWgamma)*pow(rLocal + NFWrs,3)*rhoLocal/pow(NFWrs,3);
  //return norm / (pow(r/NFWrs, NFWgamma)*pow(1 + r/NFWrs, 3.0-NFWgamma));
  return rhoLocal*pow(r/rLocal, -NFWgamma)* pow((1+rLocal/NFWrs)/(1+ r/NFWrs), 3-NFWgamma);


}

double rhoEIN(double r){
  //DM density as a function of radial coordinate from GC for Einasto profile
  return EINnorm *exp(-2.0/EINalpha*(pow(r/EINrs, EINalpha)- 1.0));

}

double EINlos(double d, double l, double b){
  //Einasto profile line of sight integrand
  double conversion =3.08567758e18 * 1e3;
  double r = Gr(d,l,b);
  double rho = rhoEIN(r);

  return pow(rho,2)*conversion;
}

double EINlosWrapper(double d, void* params){
  //wrapper for gsl integration
  double *x = (double*)params;
  double l = x[0];
  double b = x[1];

  return EINlos(d, l, b);
}

double JEIN(double l, double b){
  //Returns J factor for Einasto profile by calculating integral along line of sight
  EINnorm = rhoLocal * exp(2.0/EINalpha*(pow(rLocal/EINrs, EINalpha)- 1.0));
  gsl_function F;
  F.function = &EINlosWrapper;
  double params[2];
  params[0] = l;
  params[1] = b;
  F.params = params;
  double result, error;
  gsl_integration_workspace * w 
    = gsl_integration_workspace_alloc (1000);
  gsl_integration_qag (&F, 0, 1000, 0, 1e-3, 15,
			 1000, w, &result, &error); 
    gsl_integration_workspace_free (w);
  return cos(b)*result;
}

//--------------------------------
//Interpolation routines for fast calculation of J factors
//--------------------------------

gsl_interp_accel *EINacc ;
gsl_spline *EINspline ;
      
gsl_interp_accel *NFWacc ;
gsl_spline *NFWspline ;

void JNFW_fast_free(void){
  gsl_spline_free (NFWspline);
  gsl_interp_accel_free (NFWacc);
}  
  
void JEIN_fast_free(void){
  gsl_spline_free (EINspline);
  gsl_interp_accel_free (EINacc);
}

void JEIN_fast_init(int N, double theta_min_, double theta_max_){
  //loop over N values of cos(theta)
  double* theta_vals = calloc(N, sizeof(double));
  double* J_vals = calloc(N, sizeof(double));
  int i;
  double theta_min = theta_min_ * M_PI/180.0;
  double theta_max = theta_max_ * M_PI/180.0;
  for ( i = 0; i < N ; i++){
    //cos(theta) = cos(b) . cos(l)
    //setting l = 0 
    theta_vals[i] = cos(theta_max) + i*(cos(theta_min) - cos(theta_max))/(N-1);
    double l = 0;
    double b = acos(theta_vals[i]);
    J_vals[i] = JEIN(l, b);
    
  } 

  //init interpolator
  EINacc =  gsl_interp_accel_alloc ();
  EINspline = gsl_spline_alloc (gsl_interp_cspline, N);
  gsl_spline_init (EINspline, theta_vals, J_vals, N);
}

double JEIN_fast(double l, double b){
  double cos_theta = fabs(cos(l)*cos(b));
  return gsl_spline_eval (EINspline, cos_theta, EINacc); 
}

void JNFW_fast_init(int N, double theta_min_, double theta_max_){
  //loop over N values of cos(theta)
  double* theta_vals = calloc(N, sizeof(double));
  double* J_vals = calloc(N, sizeof(double));
  int i;
  double theta_min = theta_min_ * M_PI/180.0;
  double theta_max = theta_max_ * M_PI/180.0;
  for ( i = 0; i < N ; i++){
    //cos(theta) = cos(b) . cos(l)
    //setting l = 0 
    theta_vals[i] = cos(theta_max) + i*(cos(theta_min) - cos(theta_max))/(N-1);
    double l = 0;
    double b = acos(theta_vals[i]);
    J_vals[i] = JNFW(l, b);
    
  } 

  //init interpolator
  NFWacc =  gsl_interp_accel_alloc ();
  NFWspline = gsl_spline_alloc (gsl_interp_cspline, N);
  gsl_spline_init (NFWspline, theta_vals, J_vals, N);
}

double JNFW_fast(double l, double b){
  double cos_theta = fabs(cos(l)*cos(b));
  return gsl_spline_eval (NFWspline, cos_theta, NFWacc); 
}

//--------------------------------
//Integrators to calculate J factor for observation region
//--------------------------------

int (*gslRegionFuncWrapper)(double, double, void*);
double (*gslJFuncWrapper)(double, double);

double Jintegrator(int (*regionFunc)(double, double, void*),  double* params, double Jfun(double, double), double* lrange, double* brange, int samples, double* solidAngleOut){
  //Simple MonteCarlo integrator. Calculates solid angle and J factor from single set of MC samples.
  //params stores variables for integration region function should be in radians.
  //lrange and brange should be in degrees
  double solidAngle=0;
  double Jfactor=0;
  //init RNG

  const gsl_rng_type * T;
  gsl_rng * r;

  gsl_rng_env_setup();
  
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  srand (time(NULL));
  int seed = rand() % 32000;
  gsl_rng_set(r,seed);

  //loop over requested number of trials
  int i;
  double count=0;
  for (i = 0; i < samples; i++){

    //generate coordinate
    double l = M_PI/ 180.0*(lrange[0] + (lrange[1] - lrange[0])*gsl_rng_uniform (r));
    double b = M_PI/ 180.0*(brange[0] + (brange[1] - brange[0])*gsl_rng_uniform (r));

    //check if it is in the region
    int ok = (*regionFunc)(l, b, params);

    //increment Jfactor and solid angle
    if (ok){
      //printf("l=%.5E,b=%.5E: %d\n", l*180/M_PI, b*180/M_PI, ok);
      solidAngle += 1.0;
      // printf("%lf %lf\n", l*180/M_PI, b*180/M_PI);
      Jfactor += Jfun(l,b);
      count +=1.0;
    }    
  }
  
  //average J factor and solid angle
  solidAngle = solidAngle/samples*(brange[1]-brange[0])*(lrange[1] - lrange[0])*pow(M_PI/180, 2);
  *solidAngleOut = solidAngle;
  Jfactor = Jfactor/count*solidAngle;

  return Jfactor;
}

double gslMonteCarloWrapperSolidAngle(double *k, size_t dim, void *params){
  //wrapper for GSL montecarlo integration of soild angle

  //convert to b, l
  double b,l;
  l = M_PI/ 180.0*k[0];
  b = M_PI/ 180.0*k[1];
  l = k[0];
  b = k[1];
  //evaluate region runction
  int ok = (*gslRegionFuncWrapper)(l, b, params);

  //evaluate J function 
  double J;
  if (ok){
    J = 1.0;
  }
  else	
    J = 0.0;

  return J;
}


double gslMonteCarloWrapper(double *k, size_t dim, void *params){
  //wrapper for gsl montecarlo integration of J factor
  //convert to b, l
  double b,l;
  l = M_PI/ 180.0*k[0];
  b = M_PI/ 180.0*k[1];
  l= k[0];
  b = k[1];
  //evaluate region runction
  int ok = (*gslRegionFuncWrapper)(l, b, params);

  //evaluate J function 
  double J;
  if (ok){
     J = (*gslJFuncWrapper)(l,b);
  }
  else	
    J = 0.0;

  return J;
}



void JVEGASintegrator(int (*regionFunc)(double, double, void*), double* params, double (*Jfun)(double, double), double* lrange, double* brange, int samples, int doSolidAngle, double *solidAngleOut, double *result){
  //Integrates J factor in region defined by region func using the VEGAS method
  //params stores variables for region definition in radians
  //l and b ranges given in degrees
  //solid angle requires separate calculation and is optional.

  //set up functions for on region
  gslJFuncWrapper = Jfun;
  gslRegionFuncWrapper = regionFunc;

  //init gsl vegas algorithm
  double res, err;
  const gsl_rng_type *T;
  gsl_rng *r;
  gsl_monte_function G = { &gslMonteCarloWrapper, 2, params };
  size_t calls = samples;

  //Convert from degrees to radians for integration
  double xl[2], xu[2];
  xl[0] =  M_PI/ 180.0*lrange[0];
  xl[1] =  M_PI/ 180.0*brange[0];
  xu[0] =  M_PI/ 180.0*lrange[1];
  xu[1] =  M_PI/ 180.0*brange[1];

  //Seed the random number generator
  gsl_rng_env_setup ();
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  srand (time(NULL));
  int seed = rand() % 32000;
  gsl_rng_set(r,seed);

  gsl_monte_vegas_state *s = gsl_monte_vegas_alloc(2);
 
  //VEGAS warm up step
  //printf("starting VEGAS\n");
  gsl_monte_vegas_integrate (&G, xl, xu, 2, calls/5, r, s,
			     &res, &err);

  //Continue VEGAS integration until convergence reached
  do
    {
      gsl_monte_vegas_integrate (&G, xl, xu, 2, calls/5, r, s,
				 &res, &err);
    }
    while (fabs (gsl_monte_vegas_chisq (s) - 1.0) > 0.5);


  *result = res;
  gsl_monte_vegas_free (s);  
  
  //Repeat integration for soild angle if required
  if (doSolidAngle){
    //OFF region
    G.f = &gslMonteCarloWrapperSolidAngle;
    s = gsl_monte_vegas_alloc(2);
    
    gsl_monte_vegas_integrate (&G, xl, xu, 2, calls/5, r, s, &res, &err);
    
    do
      {
	gsl_monte_vegas_integrate (&G, xl, xu, 2, calls/5, r, s,
				   &res, &err);
      }
    while (fabs (gsl_monte_vegas_chisq (s) - 1.0) > 0.5);
    *solidAngleOut = res;
    gsl_monte_vegas_free (s);    
  }  

  gsl_rng_free(r);
 
}

void Jintegrator_two(int (*regionFuncON)(double, double, void*),int (*regionFuncOFF)(double, double, void*), double* params, double Jfun(double, double), double* lrange, double* brange, int samples, double* solidAngleOut, double* result){
  //performs simple MC integration but allows 2 regions to be calculated from a single set of samples
  //useful for quick results with two regions

  double solidAngleON=0;
  double JfactorON=0;
  double solidAngleOFF=0;
  double JfactorOFF=0;
  //init RNG

  const gsl_rng_type * T;
  gsl_rng * r;

  gsl_rng_env_setup();
  
  T = gsl_rng_default;
  r = gsl_rng_alloc (T);
  srand (time(NULL));
  int seed = rand() % 32000;
  gsl_rng_set(r,seed);

  //loop over requested number of trials
  int i;
  double countON=0;
  double countOFF=0;
  for (i = 0; i < samples; i++){

    //generate coordinate
    double l = M_PI/ 180.0*(lrange[0] + (lrange[1] - lrange[0])*gsl_rng_uniform (r));
    double b = M_PI/ 180.0*(brange[0] + (brange[1] - brange[0])*gsl_rng_uniform (r));

    //check if it is in the region
    int okOn = (*regionFuncON)(l, b, params);
    int okOff = (*regionFuncOFF)(l, b, params);
    //increment Jfactor and solid angle
    if (okOn){
      //printf("l=%.5E,b=%.5E: %d\n", l*180/M_PI, b*180/M_PI, ok);
      solidAngleON += 1.0;
      // printf("%lf %lf\n", l*180/M_PI, b*180/M_PI);
      JfactorON += Jfun(l,b);
      countON +=1.0;
    }    
    if (okOff){
      //printf("l=%.5E,b=%.5E: %d\n", l*180/M_PI, b*180/M_PI, ok);
      solidAngleOFF += 1.0;
      // printf("%lf %lf\n", l*180/M_PI, b*180/M_PI);
      JfactorOFF += Jfun(l,b);
      countOFF +=1.0;
    }   

  }
  
  //average J factor solid angle

  solidAngleON = solidAngleON/samples*(brange[1]-brange[0])*(lrange[1] - lrange[0])*pow(M_PI/180, 2);
  solidAngleOFF = solidAngleOFF/samples*(brange[1]-brange[0])*(lrange[1] - lrange[0])*pow(M_PI/180, 2);
  solidAngleOut[0] = solidAngleON;
  solidAngleOut[1] = solidAngleOFF;
  result[0] = JfactorON/countON*solidAngleON;
  result[1] = JfactorOFF/countOFF*solidAngleOFF;

}

