
CC=gcc
CFLAGS=-O3
CLIBS=-lgsl -lgslcblas

OBJECTS=J_factor_calc.o regions.o

.PHONY: clean

main: main.c $(OBJECTS)
	$(CC) $(CFLAGS) -o main main.c $(OBJECTS) -lgsl -lgslcblas
%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f *.o main