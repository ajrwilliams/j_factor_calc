/*  
-------------------------------------------------
This file is part of J factor calc 
Andrew Williams, 2016
-------------------------------------------------
*/

#include "J_factor_calc.h"
#include "regions.h"
#include "math.h"

//--------------------------------
// Functions that define observation regions should be added here
//--------------------------------


int ONregion(double l, double b, void *v){
  //Define an ON region as intersection of a disc centered on the galactic plane with annulus off set and a mask for the galactic plane.

  double *params = (double*)v;

  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region

  double r = offSetRadius(l, b, ringOffset);
  double gcr = galacticRadius(l, b);
   
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && InAnnulus(gcr, 0, ringRcut))
    return 1;
  else
    return 0;
}

int OFFregion(double l, double b, void *v){
  //Define an OFF region as annulus off set from the galactic centre excluding a disc centred on the GC (i.e. the ON regions) and a mask for the galactic plane.
  double *params = (double*)v;
  
  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region

  double r = offSetRadius(l, b, ringOffset);
  double gcr = galacticRadius(l, b);
  
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && !InAnnulus(gcr, 0, ringRcut))
    return 1;
  else
    return 0;
}

int OFFregionUpper(double l, double b, void *v){
  //OFF region as above but only region in upper half of large annulus
  double *params = (double*)v;
  
  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region

  double r = offSetRadius(l, b, ringOffset);
  double gcr = galacticRadius(l, b);
  
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && !InAnnulus(gcr, 0, ringRcut) && b > ringOffset)
    return 1;
  else
    return 0;
}

int OFFregionLower(double l, double b, void *v){
  //OFF region as above but only lower half of annulus
  double *params = (double*)v;
  
  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region

  double r = offSetRadius(l, b, ringOffset);
  double gcr = galacticRadius(l, b);
  
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && !InAnnulus(gcr, 0, ringRcut) && b < ringOffset)
    return 1;
  else
    return 0;
}


int ONregion_inner(double l, double b , void* v ){
  //ON region but with tighter cut on radius from galactic centre
   double *params = (double*)v;
  
  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region
  double innerONcut = params[5]; //radius defining innermost ON region

  double r = acos(cos(l)*cos(b - ringOffset));
  double gcr = acos(cos(l)*cos(b));
  
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && InAnnulus(gcr, 0, innerONcut))
    return 1;
  else
    return 0;
  
}
int ONregion_outer(double l, double b, void* v){
  //ON region as above but excluding innermost region as set by innerONcut
    double *params = (double*)v;
  
  double ringOffset = params[0]; //offset between ring centre and galactic centre
  double ringR1 = params[1]; //Inner radius
  double ringR2 = params[2]; //Outer radius
  double bmask = params[3]; //mask over galactic plane
  double ringRcut = params[4]; //galactic radius for ON region
  double innerONcut = params[5]; //radius defining innermost ON region

  double r = acos(cos(l)*cos(b - ringOffset));
  double gcr = acos(cos(l)*cos(b));
  
  if (InAnnulus(r, ringR1, ringR2) && GalacticMask(b, bmask) && InAnnulus(gcr, innerONcut, ringRcut))
    return 1;
  else
    return 0;
 
}


