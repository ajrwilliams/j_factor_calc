/*  
-------------------------------------------------
This file is part of J factor calc 
Andrew Williams, 2016
-------------------------------------------------
*/

//--------------------------------
//Functions to define observations regions
//--------------------------------
int ONregion(double l, double b, void* v);
int ONregion_inner(double l, double b, void* v);
int ONregion_outer(double l, double b, void* v);
int OFFregion(double l, double b, void* v);
int OFFregionUpper(double l, double b, void* v);
int OFFregionLower(double l, double b, void* v);

//Define new observation regions below
